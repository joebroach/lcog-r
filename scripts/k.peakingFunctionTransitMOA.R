#############################################################################################################################################################################################################
##                                                                                                                                                                                                         ##
## k.peakingFunctionTransitMOA.R                                                                                                                                                                           ##
## Created by PGB 5/2013                                                                                                                                                                                   ##
##                                                                                                                                                                                                         ##
## Contains a set of functions for peaking mode of access for transit                                                                                                                                      ##
##                                                                                                                                                                                                         ##
## Function usage:                                                                                                                                                                                         ##
## > transitPeakingMOA(emmeOut,emmeMatNumPAw,emmeMatNamePAw,emmeMatDescPAw,                                                                                                                                ##
##                             emmeMatNumAPw,emmeMatNameAPw,emmeMatDescAPw,                                                                                                                                ##
##                             emmeMatNumOPw,emmeMatNameOPw,emmeMatDescOPw,                                                                                                                                ##
##                             emmeMatNumPAp,emmeMatNamePAp,emmeMatDescPAp,                                                                                                                                ##
##                             emmeMatNumAPp,emmeMatNameAPp,emmeMatDescAPp,                                                                                                                                ##
##                             emmeMatNumOPp,emmeMatNameOPp,emmeMatDescOPp)                                                                                                                                ##
##   where,                                                                                                                                                                                                ##
##    emmeOut        : OPTIONAL : 'T' or 'F' whether Emme demand table is created                                 -- 'T' will output trip table to Emme bank defined by variable 'bank'                    ##
##                                                                                                                   if value of 'T', requires that emmeMatNum, emmeMatName, and emmeMatDesc be defined    ##
##                                                                                                                   'F' is default                                                                        ##
##    emmeMatNumPAw  : OPTIONAL : matrix location in Emme bank to write Peak P-to-A walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatPAw must be value between 1 and 150         ##
##    emmeMatNamePAw : OPTIONAL : 6-string matrix name in Emme bank for Peak P-to-A walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatNamePAw must be 1 to 6 character string     ##
##    emmeMatDescPAw : OPTIONAL : description of matrix in Emme bank for Peak P-to-A walk to transit trips table  -- if emmeOut variable is set to 'T', emmeMatDescPAw must be upto 40 character string    ##
##    emmeMatNumAPw  : OPTIONAL : matrix location in Emme bank to write Peak A-to-P walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatAPw must be value between 1 and 150         ##
##    emmeMatNameAPw : OPTIONAL : 6-string matrix name in Emme bank for Peak A-to-P walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatNameAPw must be 1 to 6 character string     ##
##    emmeMatDescAPw : OPTIONAL : description of matrix in Emme bank for Peak A-to-P walk to transit trips table  -- if emmeOut variable is set to 'T', emmeMatDescAPw must be upto 40 character string    ##
##    emmeMatNumOPw  : OPTIONAL : matrix location in Emme bank to write OffPeak walk to transit trips table       -- if emmeOut variable is set to 'T', emmeMatOPw must be value between 1 and 150         ##
##    emmeMatNameOPw : OPTIONAL : 6-string matrix name in Emme bank for OffPeak walk to transit trips table       -- if emmeOut variable is set to 'T', emmeMatNameOPw must be 1 to 6 character string     ##
##    emmeMatDescOPw : OPTIONAL : description of matrix in Emme bank for OffPeak walk to transit trips table      -- if emmeOut variable is set to 'T', emmeMatDescOPw must be upto 40 character string    ##
##    emmeMatNumPAp  : OPTIONAL : matrix location in Emme bank to write Peak P-to-A drive to transit trips table  -- if emmeOut variable is set to 'T', emmeMatPAp must be value between 1 and 150         ##
##    emmeMatNamePAp : OPTIONAL : 6-string matrix name in Emme bank for Peak P-to-A drive to transit trips table  -- if emmeOut variable is set to 'T', emmeMatNamePAp must be 1 to 6 character string     ##
##    emmeMatDescPAp : OPTIONAL : description of matrix in Emme bank for Peak P-to-A drive to transit trips table -- if emmeOut variable is set to 'T', emmeMatDescPAp must be upto 40 character string    ##
##    emmeMatNumAPp  : OPTIONAL : matrix location in Emme bank to write Peak A-to-P drive transit trips table     -- if emmeOut variable is set to 'T', emmeMatAPp must be value between 1 and 150         ##
##    emmeMatNameAPp : OPTIONAL : 6-string matrix name in Emme bank for Peak A-to-P drive trips table             -- if emmeOut variable is set to 'T', emmeMatNameAPp must be 1 to 6 character string     ##
##    emmeMatDescAPp : OPTIONAL : description of matrix in Emme bank for Peak A-to-P drive trips table            -- if emmeOut variable is set to 'T', emmeMatDescAPp must be upto 40 character string    ##
##    emmeMatNumOPp  : OPTIONAL : matrix location in Emme bank to write OffPeak drive transit trips table         -- if emmeOut variable is set to 'T', emmeMatOPp must be value between 1 and 150         ##
##    emmeMatNameOPp : OPTIONAL : 6-string matrix name in Emme bank for OffPeak drive transit trips table         -- if emmeOut variable is set to 'T', emmeMatNameOPp must be 1 to 6 character string     ##
##    emmeMatDescOPp : OPTIONAL : description of matrix in Emme bank for OffPeak drive transit trips table        -- if emmeOut variable is set to 'T', emmeMatDescOPp must be upto 40 character string    ##
##                                                                                                                                                                                                         ##
## Examples:                                                                                                                                                                                               ##
## > transitPeakingMOA()                                                                                                                                                                                   ##
## > transitPeakingMOA(emmeOut=T,emmeMatNumPAw=110,emmeMatNamePAw='PAwtr',emmeMatDescPAw='Peak PA Walk to Transit Trips',                                                                                    ##
##                               emmeMatNumAPw=111,emmeMatNameAPw='APwtr',emmeMatDescAPw='Peak AP Walk to Transit Trips',                                                                                    ##
##                               emmeMatNumOPw=112,emmeMatNameOPw='OPwtr',emmeMatDescOPw='OffPeak Walk to Transit Trips',                                                                                    ##
##                               emmeMatNumPAp=113,emmeMatNamePAp='PAptr',emmeMatDescPAp='Peak PA Drive to Transit Trips',                                                                                   ##
##                               emmeMatNumAPp=114,emmeMatNameAPp='APptr',emmeMatDescAPp='Peak AP Drive to Transit Trips',                                                                                   ##
##                               emmeMatNumOPp=115,emmeMatNameOPp='OPptr',emmeMatDescOPp='OffPeak Drive to Transit Trips')                                                                                   ##
##                                                                                                                                                                                                         ##
#############################################################################################################################################################################################################

###########################################################
##             Calculating Transit Trips                 ##
###########################################################
transitPeakingMOA <- function(emmeOut=F,emmeMatNumPAw=0,emmeMatNamePAw='',emmeMatDescPAw='',
                                        emmeMatNumAPw=0,emmeMatNameAPw='',emmeMatDescAPw='',
                                        emmeMatNumOPw=0,emmeMatNameOPw='',emmeMatDescOPw='',
                                        emmeMatNumPAp=0,emmeMatNamePAp='',emmeMatDescPAp='',
                                        emmeMatNumAPp=0,emmeMatNameAPp='',emmeMatDescAPp='',
                                        emmeMatNumOPp=0,emmeMatNameOPp='',emmeMatDescOPp='') {

  # Check Emme variables. Ends function and reports error message if variables are incorrect.
  stp <- emmeCheck(emmeOut,emmeMatNumPAw,emmeMatNamePAw,emmeMatDescPAw)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumAPw,emmeMatNameAPw,emmeMatDescAPw)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumOPw,emmeMatNameOPw,emmeMatDescOPw)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumPAp,emmeMatNamePAp,emmeMatDescPAp)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumAPp,emmeMatNameAPp,emmeMatDescAPp)
  if (stp > 0) { stop(call.=FALSE) }
    stp <- emmeCheck(emmeOut,emmeMatNumOPp,emmeMatNameOPp,emmeMatDescOPp)
  if (stp > 0) { stop(call.=FALSE) }

  print ("Peaking Mode of Access for Transit")

  ### Calculations for Drive to Transit (PnR) ###
       # PKAD peak Direction
       mf.pkdir.prtrtd <- (mf.hwprtrtd) * (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18]) +
                          (mf.hoprtrtd) * (peakHBO.TRANSIT.PA[8] + peakHBO.TRANSIT.PA[9] + peakHBO.TRANSIT.AP[17] + peakHBO.TRANSIT.AP[18]) +
                          (mf.hsprtrtd) * (peakHBS.TRANSIT.PA[8] + peakHBS.TRANSIT.PA[9] + peakHBS.TRANSIT.AP[17] + peakHBS.TRANSIT.AP[18]) +
                          (mf.hrprtrtd) * (peakHBR.TRANSIT.PA[8] + peakHBR.TRANSIT.PA[9] + peakHBR.TRANSIT.AP[17] + peakHBR.TRANSIT.AP[18]) +
                          (mf.hcprtrtd) * (peakCollege.TRANSIT.PA[8] + peakCollege.TRANSIT.PA[9] + peakCollege.TRANSIT.AP[17] + peakCollege.TRANSIT.AP[18])

       ### only non-passenger trips to/from PDX are HBW
       mf.pkdir.prtrtd[pdxtaz,] <- mf.hwprtrtd[pdxtaz,] * (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18])
       mf.pkdir.prtrtd[,pdxtaz] <- mf.hwprtrtd[,pdxtaz] * (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18])

       # PKAD off-peak Direction
       ### Calculations for Drive to Transit (PnR) ###
       mf.opdir.prtrtd <- (mf.hwprtrtd) * (peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]) +
                          (mf.hoprtrtd) * (peakHBO.TRANSIT.AP[8] + peakHBO.TRANSIT.AP[9] + peakHBO.TRANSIT.PA[17] + peakHBO.TRANSIT.PA[18]) +
                          (mf.hsprtrtd) * (peakHBS.TRANSIT.AP[8] + peakHBS.TRANSIT.AP[9] + peakHBS.TRANSIT.PA[17] + peakHBS.TRANSIT.PA[18]) +
                          (mf.hrprtrtd) * (peakHBR.TRANSIT.AP[8] + peakHBR.TRANSIT.AP[9] + peakHBR.TRANSIT.PA[17] + peakHBR.TRANSIT.PA[18]) +
                          (mf.hcprtrtd) * (peakCollege.TRANSIT.AP[8] + peakCollege.TRANSIT.AP[9] + peakCollege.TRANSIT.PA[17] + peakCollege.TRANSIT.PA[18])

       ### only non-passenger trips to/from PDX are HBW
       mf.opdir.prtrtd[pdxtaz,] <- mf.hwprtrtd[pdxtaz,] * (peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18])
       mf.opdir.prtrtd[,pdxtaz] <- mf.hwprtrtd[,pdxtaz] * (peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18])

       #Calculate PKAD Drive to Transit Trips
       mf.PKAD.PA.prtrtd <- mf.opdir.prtrtd
       mf.PKAD.AP.prtrtd <- t(mf.pkdir.prtrtd)

       #Calculate OPAD Drive to Transit Trips
       mf.OPAD.prtrtd <- (mf.hwprtrtd + 
                          mf.hsprtrtd +
                          mf.hrprtrtd +
                          mf.hoprtrtd +
                          mf.hcprtrtd) -
                          mf.pkdir.prtrtd - mf.opdir.prtrtd

       ### only non-passenger trips to/from PDX are HBW
       mf.OPAD.prtrtd[pdxtaz,] <- mf.hwprtrtd[pdxtaz,] * 
                                  (1.0 - (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18] + peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]))
       mf.OPAD.prtrtd[,pdxtaz] <- mf.hwprtrtd[,pdxtaz] * 
                                  (1.0 - (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18] + peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]))

  ### Calculations for Walk to Transit ###
       # PKAD peak Direction
       mf.pkdir.tr     <- (mf.hwtr)   * (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18]) +
                          (mf.hotr)   * (peakHBO.TRANSIT.PA[8] + peakHBO.TRANSIT.PA[9] + peakHBO.TRANSIT.AP[17] + peakHBO.TRANSIT.AP[18]) +
                          (mf.hstr)   * (peakHBS.TRANSIT.PA[8] + peakHBS.TRANSIT.PA[9] + peakHBS.TRANSIT.AP[17] + peakHBS.TRANSIT.AP[18]) +
                          (mf.hrtr)   * (peakHBR.TRANSIT.PA[8] + peakHBR.TRANSIT.PA[9] + peakHBR.TRANSIT.AP[17] + peakHBR.TRANSIT.AP[18]) +
                          (mf.nhwtr)  * (peakNHW.TRANSIT.PA[8] + peakNHW.TRANSIT.PA[9] + peakNHW.TRANSIT.AP[17] + peakNHW.TRANSIT.AP[18]) +
                          (mf.nhnwtr) * (peakNHNW.TRANSIT.OD[8] + peakNHNW.TRANSIT.OD[9] + peakNHNW.TRANSIT.OD[17] + peakNHNW.TRANSIT.OD[18]) / 2

       # Add college and school trips
       mf.pkdir.tr     <- (mf.hctr)   * (peakCollege.TRANSIT.PA[8] + peakCollege.TRANSIT.PA[9] + peakCollege.TRANSIT.AP[17] + peakCollege.TRANSIT.AP[18]) +
                          (mf.schtr)  * (peakSchool.TRANSIT.PA[8] + peakSchool.TRANSIT.PA[9] + peakSchool.TRANSIT.AP[17] + peakSchool.TRANSIT.AP[18]) +
                          (mf.pkdir.tr)

       ### only non-passenger trips to/from PDX are HBW
       mf.pkdir.tr[pdxtaz,] <- (mf.hwtr[pdxtaz,] + mf.hwprtrtd[pdxtaz,]) * (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18])
       mf.pkdir.tr[,pdxtaz] <- (mf.hwtr[,pdxtaz] + mf.hwprtrtd[,pdxtaz]) * (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18])

       # PKAD off-peak Direction
       mf.opdir.tr     <- (mf.hwtr)   * (peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]) +
                          (mf.hotr)   * (peakHBO.TRANSIT.AP[8] + peakHBO.TRANSIT.AP[9] + peakHBO.TRANSIT.PA[17] + peakHBO.TRANSIT.PA[18]) +
                          (mf.hstr)   * (peakHBS.TRANSIT.AP[8] + peakHBS.TRANSIT.AP[9] + peakHBS.TRANSIT.PA[17] + peakHBS.TRANSIT.PA[18]) +
                          (mf.hrtr)   * (peakHBR.TRANSIT.AP[8] + peakHBR.TRANSIT.AP[9] + peakHBR.TRANSIT.PA[17] + peakHBR.TRANSIT.PA[18]) +
                          (mf.nhwtr)  * (peakNHW.TRANSIT.AP[8] + peakNHW.TRANSIT.AP[9] + peakNHW.TRANSIT.PA[17] + peakNHW.TRANSIT.PA[18]) +
                          (mf.nhnwtr) * (peakNHNW.TRANSIT.OD[8] + peakNHNW.TRANSIT.OD[9] + peakNHNW.TRANSIT.OD[17] + peakNHNW.TRANSIT.OD[18]) / 2

       # Add college and school trips
       mf.opdir.tr     <- (mf.hctr)   * (peakCollege.TRANSIT.AP[8] + peakCollege.TRANSIT.AP[9] + peakCollege.TRANSIT.PA[17] + peakCollege.TRANSIT.PA[18]) +
                          (mf.schtr)  * (peakSchool.TRANSIT.AP[8] + peakSchool.TRANSIT.AP[9] + peakSchool.TRANSIT.PA[17] + peakSchool.TRANSIT.PA[18]) +
                          (mf.opdir.tr)

       ### only non-passenger trips to/from PDX are HBW
       mf.opdir.tr[pdxtaz,] <- mf.hwtr[pdxtaz,] * (peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18])
       mf.opdir.tr[,pdxtaz] <- mf.hwtr[,pdxtaz] * (peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18])

       #Calculate PKAD Walk to Transit Trips
       mf.PKAD.PA.tr <- mf.opdir.tr
       mf.PKAD.AP.tr <- t(mf.pkdir.tr)

       #Calculate OPAD Walk to Transit Trips
       mf.OPAD.tr <- (mf.hwtr +
                      mf.hstr +
                      mf.hrtr +
                      mf.hotr +
                      mf.nhwtr +
                      mf.nhnwtr +
                      mf.hctr + mf.schtr) -
                      mf.pkdir.tr - mf.opdir.tr

       ### only non-passenger trips to/from PDX are HBW
       mf.OPAD.tr[pdxtaz,] <- mf.hwtr[pdxtaz,] * 
                              (1.0 - (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18] + peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]))
       mf.OPAD.tr[,pdxtaz] <- mf.hwtr[,pdxtaz] * 
                              (1.0 - (peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18] + peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]))


       # Add airport passengers
      dataList             <- read.csv(paste(airport_dir,"lrt_trips_PKAD.csv",sep='/'),sep=",",header=F)
      mf.airportTrips.PKAD <- t(array(dataList[,3], dim=c(numzones,numzones)))

      dataList             <- read.csv(paste(airport_dir,"lrt_trips_OPAD.csv",sep='/'),sep=",",header=F)
      mf.airportTrips.OPAD <- t(array(dataList[,3], dim=c(numzones,numzones)))

      mf.PKAD.PA.tr        <- mf.PKAD.PA.tr + mf.airportTrips.PKAD
      mf.OPAD.tr           <- mf.OPAD.tr + mf.airportTrips.OPAD

  # Output trip table to Emme if defined
  if (isTRUE(emmeOut)) { 
     print("Creating Emme O->D tables for PKAD & OPAD transit trips")
     writeEmme(mf.PKAD.PA.tr,assignbank,emmeMatNamePAw,emmeMatDescPAw,emmeMatNumPAw)
     writeEmme(mf.PKAD.AP.tr,assignbank,emmeMatNameAPw,emmeMatDescAPw,emmeMatNumAPw)
     writeEmme(mf.OPAD.tr,assignbank,emmeMatNameOPw,emmeMatDescOPw,emmeMatNumOPw)
     writeEmme(mf.PKAD.PA.prtrtd,assignbank,emmeMatNamePAp,emmeMatDescPAp,emmeMatNumPAp)
     writeEmme(mf.PKAD.AP.prtrtd,assignbank,emmeMatNameAPp,emmeMatDescAPp,emmeMatNumAPp)
     writeEmme(mf.OPAD.prtrtd,assignbank,emmeMatNameOPp,emmeMatDescOPp,emmeMatNumOPp)
  }

  mf.distemp <<- 0
}
