getwd()
load("data/mf.hcprtrtd.pk.dat")
load("data/mf.hcprtrtl.pk.dat")
load("data/mf.hcprtr.pk.dat")


sum(mf.hcprtrtd.pk[,397:403])

mf.pkdir.prtrtd <- (mf.hcprtrtd.pk) * (peakCollege.TRANSIT.PA[7] + peakCollege.TRANSIT.PA[8] + peakCollege.TRANSIT.PA[9] + peakCollege.TRANSIT.AP[16] + peakCollege.TRANSIT.AP[17] + peakCollege.TRANSIT.AP[18])

sum(mf.pkdir.prtrtd[,397:403])

mf.pkdir.prtrtd.t <- t(mf.pkdir.prtrtd)

sum(mf.pkdir.prtrtd.t[397:403,])


sum(mf.hcprtrtd.pk[,400])
sum(mf.hcprtrtl.pk[,400])
sum(mf.hcprtr.pk[,400])

sum(mf.hcprtrtd.pk[400,])
sum(mf.hcprtrtl.pk[400,])
sum(mf.hcprtr.pk[400,])

load("data/bestLotsArray.RData")
load("data/bestLotsUtilArray.RData")
load("data/bestInfLotsArray.RData")
load("data/bestInfLotsUtilArray.RData")

head(bestLotsArray)
str(bestLotsArray)

bestLotsArray[150, 400, 1:2]
bestLotsUtilArray[150, 400, 1:2]
bestInfLotsArray[150, 400, 1:2]
bestInfLotsUtilArray[150, 400, 1:2]

calc_probs_mnl <- function(utils) {
  sum_exp <- sum(sapply(utils, exp))
  i = 1
  probs <- c()
  for (v in utils) {
    probs[i] <- exp(v) / sum_exp
    i = i + 1
  }
  return(as.vector(probs))
} 

sum(calc_probs_mnl(c(0,1,2)))

#####
a <- array(1:4, dim=c(2, 2))
a
a[2, 1]
