# Checking on UO trips table
df <- read.csv("data/trips.csv")
head(df)

# Park/Kiss and ride trips to/from UO
swap_zones <- c(397: 403)
pnr_modes <- c(11, 12)
pnr <- subset(df, tripMode %in% pnr_modes & 
              originPurpose == -1 &  
              destinationTaz %in% swap_zones)  # 221 pnr trips
table(pnr$destinationPurpose)  # All college, no work trips

pnr.ap <- subset(df, tripMode %in% pnr_modes & 
                 originTaz %in% swap_zones &  
                 destinationPurpose == -1)  # 96 pnr trips
table(pnr.ap$originPurpose)  # 95 college, 1 work trip

# pnr trip OD distribution
pnr.hh <- subset(df, hh_id %in% unique(pnr$hh_id))

## manual review of trip table doesn't show any obvious joint travel inta hh

write.csv(pnr, "maps/pnr_pa.csv", row.names=F)
write.csv(pnr.ap, "maps/pnr_ap.csv", row.names=F)

####

