# Work on PnR segmentation

# load the UO trips
# Load U of O tour-based model trip records
print("Load U of O tour-based model trips")
uo_trips <- read.csv("data/trips.csv", sep=",", header=TRUE)
head(uo_trips
     )

# skim off prtr trips
prtr.all <- subset(uo_trips, tripMode %in% c(11,12)) 

# load best formal PnR lots
# This is an array with best 2 lots for each TAZ pair
load("data/bestLotsArray.RData")
str(bestLotsArray)
load("data/bestLotsUtilArray.RData")
str(bestLotsUtilArray)

# e.g. 
bestLotsArray[150, 400, 1:2]
bestLotsUtilArray[150, 400, 1:2]

test.array <- array(NA, dim=c(666, 666, 3))
str(test.array)
test.array[,,-3] <- bestLotsArray
test.array[150, 400, 1:3]

# confirm that lots are rank-ordered
for (i in 1:666) {
  for (j in 1:666) {
    if (!anyNA(bestLotsArray[i, j, 1:2])) {
      test.array[i, j, 3] <- (bestLotsUtilArray[i, j, 1] >= 
        bestLotsUtilArray[i, j, 2]) 
    } 
  }
} 

min(test.array[, , 3], na.rm=T)  # =1, all are greater

# pull first (best) formal lot for each trip P->A direction
# trips must be HBC, from|to UofO on College end, and match modes and periods 
swap_zones <- c(397:403)
prtr.pa <- subset(
  prtr.all, 
  originPurpose == -1 & 
    (destinationTaz %in% swap_zones & destinationPurpose == 1))
prtr.pa$id <- 1:nrow(prtr.pa)


prtr.segmented <- prtr.pa[0,]  # match original trip df structure
# loop over trips, creating 2 new trip legs per trip
t2 <- 1 # new trip index
for (t in 1:nrow(prtr.pa)) {
  i <- prtr.pa[t, "originTaz"]
  j <- prtr.pa[t, "destinationTaz"]
  lot_zone <- bestLotsArray[i, j, 1]
  prtr.segmented[t2,] <- prtr.pa[t,]
  prtr.segmented[t2, "tripMode"] <- 1  # SOV
  prtr.segmented[t2, "destinationTaz"] <- lot_zone  # shift dest. to PnR lot
  t2 <- t2 + 1
  prtr.segmented[t2,] <- prtr.pa[t,]  # duplicate trip for 2nd leg
  prtr.segmented[t2, "originTaz"] <- lot_zone  # shift O to PnR lot for leg 2
  t2 <- t2 + 1
}
head(prtr.pa[order(prtr.pa$id),])
head(prtr.segmented[order(prtr.pa$id),])

prtr.ap <- subset(
  prtr.all, 
  originPurpose == 1 & 
    (originTaz %in% swap_zones & destinationPurpose == -1))
prtr.ap$id <- seq(222, 222 + nrow(prtr.ap) - 1)

# loop over P->A trips, creating 2 new trip legs per trip
t2 <- nrow(prtr.segmented) + 1  # new trip index; append to table
for (t in 1:nrow(prtr.ap)) {
  i <- prtr.ap[t, "originTaz"]
  j <- prtr.ap[t, "destinationTaz"]
  lot_zone <- bestLotsArray[j, i, 1]  # use P->A lot
  prtr.segmented[t2,] <- prtr.ap[t,]
  prtr.segmented[t2, "destinationTaz"] <- lot_zone  # shift dest. to PnR lot
  t2 <- t2 + 1
  prtr.segmented[t2,] <- prtr.ap[t,]  # duplicate trip for 2nd leg
  prtr.segmented[t2, "tripMode"] <- 1  # SOV
  prtr.segmented[t2, "originTaz"] <- lot_zone  # shift O to PnR lot for leg 2
  t2 <- t2 + 1
}
head(prtr.ap[order(prtr.ap$id),])
prtr.segmented[443:453, ]

