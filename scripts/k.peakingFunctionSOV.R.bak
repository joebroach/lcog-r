###########################################################################################################################################################################
##                                                                                                                                                                       ##
## k.peakingFunctionSOV.R                                                                                                                                                ##
## Created by PGB 5/2011                                                                                                                                                 ##
## Updated by PGB 7/2012                                                                                                                                                 ##
##                                                                                                                                                                       ##
## Contains a set of functions for peaking SOV, HOV, trucks, and transit                                                                                                 ##
## Also contains functions for writing O->D tables to various formats                                                                                                    ##
##                                                                                                                                                                       ##
## Function usage:                                                                                                                                                       ##
## > sovPeaking(startTable,stopTable,newReport,dynustOut,emmeOut,emmeMatNum,emmeMatName,emmeMatDesc)                                                                     ##
##   where,                                                                                                                                                              ##
##    startTable  : REQUIRED : hour to start peaking program (in 24-hr time)     -- Example: 4-6 pm peak, this field would be 16                                         ##
##    stopTable   : REQUIRED : hour to stop peaking program (in 24-hr time)      -- Example: 4-6 pm peak, this field would be 18                                         ##
##    newReport   : OPTIONAL : 'T' or 'F' whether new report file is created     -- 'T' will erase any existing district summary report file before writing new results  ##
##                                                                                  'F' (default) will append district summary results to existing report file           ##
##    dynustOut   : OPTIONAL : 'T' or 'F' whether DynusT demand table is created -- 'T' will output DynusT demand file, overwriting any existing DynusT demand file      ##
##                                                                                  'F' is default                                                                       ##
##    emmeOut     : OPTIONAL : 'T' or 'F' whether Emme demand table is created   -- 'T' will output trip table to Emme bank defined by variable 'bank'                   ##
##                                                                                  if value of 'T', requires that emmeMatNum, emmeMatName, and emmeMatDesc be defined   ##
##                                                                                  'F' is default                                                                       ##
##    emmeMatNum  : OPTIONAL : matrix location in Emme bank to write trip table  -- if emmeOut variable is set to 'T', emmeMat must be value between 1 and 150           ##
##    emmeMatName : OPTIONAL : 6-string matrix name in Emme bank for trip table  -- if emmeOut variable is set to 'T', emmeMatName must be 1 to 6 character string       ##
##    emmeMatDesc : OPTIONAL : description of matrix in Emme bank for trip table -- if emmeOut variable is set to 'T', emmeMatDesc must be upto 40 character string      ##
##                                                                                                                                                                       ##
## Examples:                                                                                                                                                             ##
## > sovPeaking(16,18)                                                                                                                                                   ##
## > sovPeaking(16,18,dynustOut=T)                                                                                                                                       ##
## > sovPeaking(16,18,newReport=T,emmeOut=T,emmeMatNum=50,emmeMatName='pm2sov',emmeMatDesc='PM2 SOV trip table')                                                         ##
##                                                                                                                                                                       ##
###########################################################################################################################################################################

###########################################################
##               Calculating SOV Trips                   ##
###########################################################
sovPeaking <- function(startTable,stopTable,newReport=F,dynustOut=F,emmeOut=F,emmeMatNum=0,emmeMatName='',emmeMatDesc='') {

  # Check Emme variables. Ends function and reports error message if variables are incorrect.
  stp <- emmeCheck(emmeOut,emmeMatNum,emmeMatName,emmeMatDesc)
  if (stp > 0) { stop(call.=FALSE) }

  # Loop through peaktab to create hourly tables for each user defined time period
  for (timePeak in (startTable + 1):stopTable) 
     {
       print (paste("Peaking SOV for ",startTime[timePeak],":00 to ",startTime[timePeak],":59",sep=""))

       timeperiod <- paste(startTime[timePeak],".",stopTime[timePeak], sep="")  # Define the start and end time of the trip table

       todTrips(timePeak)  # Run function to determine proper time of day P-A trip tables and appropriate trip table percentages

       ####---- A->P calculations ----####
       # A-P Direction
       mf.ap.sov <- (mf.hwda + mf.hwprtrtl) * peakHBW.AUTO.AP[timePeak] +
                    (mf.hoda + mf.hoprtrtl) * peakHBO.AUTO.AP[timePeak] +
                    (mf.hsda + mf.hsprtrtl) * peakHBS.AUTO.AP[timePeak] +
                    (mf.hrda + mf.hrprtrtl) * peakHBR.AUTO.AP[timePeak] +
                     mf.nhwda               * peakNHW.AUTO.AP[timePeak] +
                     mf.nhnwda              * peakNHNW.AUTO.OD[timePeak] / 2

       # Add school and college
       mf.ap.sov <- (mf.hcda + mf.hcprtrtl) * peakCollege.AUTO.AP[timePeak] + mf.ap.sov

       # Transpose AP Matrix
       mf.ap.sov <- t(mf.ap.sov)

       ####---- P->A calculations ----####
       # P->A Direction
       mf.pa.sov <- (mf.hwda + mf.hwprtrtl) * peakHBW.AUTO.PA[timePeak] +
                    (mf.hoda + mf.hoprtrtl) * peakHBO.AUTO.PA[timePeak] +
                    (mf.hsda + mf.hsprtrtl) * peakHBS.AUTO.PA[timePeak] +
                    (mf.hrda + mf.hrprtrtl) * peakHBR.AUTO.PA[timePeak] +
                     mf.nhwda               * peakNHW.AUTO.PA[timePeak] +
                     mf.nhnwda              * peakNHNW.AUTO.OD[timePeak] / 2

       # Add college trips....ADD SCHOOL TO THE HOV PORTION
       mf.pa.sov <- (mf.hcda + mf.hcprtrtl) * peakCollege.AUTO.PA[timePeak] + mf.pa.sov

       # Total SOV Vehicles (P->A + A->P)
       mf.final.sov <- mf.pa.sov + mf.ap.sov

#       print(paste("SOV Trips (no pdx / no ext) ",timePeak-1,"-",timePeak,": ",sum(mf.final.sov),sep=""))

       ####---- Finalize trip table ----####
       # Add external trips - SOV only
       # Old O-> D method : mf.pa.sov <- mf.aodext * peakExternal[timePeak] + mf.pa.sov
       # Edited to handle externals in P->A format
       # Assumes HBO AP / PA factors for external peaking
       mf.ap.awdext <- t(mf.awdext * peakEXT.AUTO.AP[timePeak])
       mf.pa.awdext <-   mf.awdext * peakEXT.AUTO.PA[timePeak]

       mf.final.sov <- mf.final.sov + mf.ap.awdext + mf.pa.awdext
       
       # If replacement of HBC trips with U of O tour-based model trips is desired...
       if (uo_replace == TRUE) {
         for (z in 1:length(uo_zones)) {
           for (i in 1:numzones) {
           # purpose = -1 (home) or 1 (university)
           # modes = 1+2 (da)
           # periods = uo_trips$period
           mf.final.sov[z,i] <- 
           mf.final.sov[i,z] <- 
           }
         }
       }

       # Trip table adjustment
       # First, determine peak table as percentage of daily trips
       # Next, compare peak table ratio to counts ratio to determine adjustment to peak table
       # Finally, apply adjustment factor to final trip table
#       peakFraction    <- round(sum(mf.final.sov) / sum(dailySOVvehicles),5)
#       peakAdjustment  <- round(peakCountBasedFactors[timePeak] / peakFraction,5)
#       mf.tempOrig     <- mf.final.sov
#       mf.final.sov    <- mf.final.sov * peakAdjustment
#       print(paste("Orig Trips: ",sum(mf.tempOrig),"    Orig Frac: ",peakFraction,"    Count Frac: ",peakCountBasedFactors[timePeak],"   Adjustment: ",peakAdjustment,"   New Frac: ",sum(mf.final.sov) / sum(dailySOVvehicles),"   Adjust Trips: ",sum(mf.final.sov),sep=""))

#       print(paste("SOV Trips (no pdx) ",timePeak-1,"-",timePeak,": ",round(sum(mf.final.sov),0),sep=""))

       # Save timeperiod-specific trip table
       assign(paste("mf.sov.",timeperiod,sep=""),mf.final.sov)
       save(list = paste("mf.sov.",timeperiod,sep=""),file=paste("mf.sov.",timeperiod,".dat",sep=""))

       rm(list=ls(pattern = "mf\\.sov"))
       rm(list=ls(pattern = "mf\\.ap\\."))
       rm(list=ls(pattern = "mf\\.pa\\."))
     }

  # Sum hourly tables for final O->D table for district summaries
  sumHourly(startTable,stopTable,'SOV')
  districtSummary(startTable,stopTable,newReport,tableType='SOV')

  # Output trip table to Emme if defined
  if (isTRUE(emmeOut)) { 
#    print(paste("Creating Emme O->D table for SOV trips from ",startTable,":00 to ",stopTable-1,":59",sep=""))
    writeEmme(mf.disTemp,assignbank,emmeMatName,emmeMatDesc,emmeMatNum)
  }

  # Output trip table to DynusT if defined -- Note : default is to set diagnol values to 0
  if (isTRUE(dynustOut)) { dynustTable(startTable,stopTable,tableType='SOV',diagnolZero=T) }

  mf.disTemp[] <<- 0
}
