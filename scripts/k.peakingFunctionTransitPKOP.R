####################################################################################################################################################################################################
##                                                                                                                                                                                                ##
## k.peakingFunctionTransitPKOP.R                                                                                                                                                                 ##
## Created by PGB 5/2011                                                                                                                                                                          ##
##                                                                                                                                                                                                ##
## Contains a set of functions for peaking SOV, HOV, trucks, and transit                                                                                                                          ##
## Also contains functions for writing O->D tables to various formats                                                                                                                             ##
##                                                                                                                                                                                                ##
## Function usage:                                                                                                                                                                                ##
## > transitPeakingPKOP(emmeOut,emmeMatNumPw,emmeMatNamePw,emmeMatDescPw,                                                                                                                         ##
##                              emmeMatNumOw,emmeMatNameOw,emmeMatDescOw,                                                                                                                         ##
##                              emmeMatNumPd,emmeMatNamePd,emmeMatDescPd,                                                                                                                         ##
##                              emmeMatNumOd,emmeMatNameOd,emmeMatDescOd,                                                                                                                         ##
##                              emmeMatNumPt,emmeMatNamePt,emmeMatDescPt,                                                                                                                         ##
##                              emmeMatNumOt,emmeMatNameOt,emmeMatDescOt)                                                                                                                         ##
##   where,                                                                                                                                                                                       ##
##    emmeOut       : OPTIONAL : 'T' or 'F' whether Emme demand table is created                          -- 'T' will output trip table to Emme bank defined by variable 'bank'                   ##
##                                                                                                           if value of 'T', requires that emmeMatNum, emmeMatName, and emmeMatDesc be defined   ##
##                                                                                                           'F' is default                                                                       ##
##    emmeMatNumPw  : OPTIONAL : matrix location in Emme bank to write PKAD walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatPw must be value between 1 and 150         ##
##    emmeMatNamePw : OPTIONAL : 6-string matrix name in Emme bank for PKAD walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatNamePw must be 1 to 6 character string     ##
##    emmeMatDescPw : OPTIONAL : description of matrix in Emme bank for PKAD walk to transit trips table  -- if emmeOut variable is set to 'T', emmeMatDescPw must be upto 40 character string    ##
##    emmeMatNumOw  : OPTIONAL : matrix location in Emme bank to write OPAD walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatOw must be value between 1 and 150         ##
##    emmeMatNameOw : OPTIONAL : 6-string matrix name in Emme bank for OPAD walk to transit trips table   -- if emmeOut variable is set to 'T', emmeMatNameOw must be 1 to 6 character string     ##
##    emmeMatDescOw : OPTIONAL : description of matrix in Emme bank for OPAD walk to transit trips table  -- if emmeOut variable is set to 'T', emmeMatDescOw must be upto 40 character string    ##
##    emmeMatNumPd  : OPTIONAL : matrix location in Emme bank to write PKAD drive to transit trips table  -- if emmeOut variable is set to 'T', emmeMatPd must be value between 1 and 150         ##
##    emmeMatNamePd : OPTIONAL : 6-string matrix name in Emme bank for PKAD drive to transit trips table  -- if emmeOut variable is set to 'T', emmeMatNamePd must be 1 to 6 character string     ##
##    emmeMatDescPd : OPTIONAL : description of matrix in Emme bank for PKAD drive to transit trips table -- if emmeOut variable is set to 'T', emmeMatDescPd must be upto 40 character string    ##
##    emmeMatNumOd  : OPTIONAL : matrix location in Emme bank to write OPAD drive to transit trips table  -- if emmeOut variable is set to 'T', emmeMatOd must be value between 1 and 150         ##
##    emmeMatNameOd : OPTIONAL : 6-string matrix name in Emme bank for OPAD drive to transit trips table  -- if emmeOut variable is set to 'T', emmeMatNameOd must be 1 to 6 character string     ##
##    emmeMatDescOd : OPTIONAL : description of matrix in Emme bank for OPAD drive to transit trips table -- if emmeOut variable is set to 'T', emmeMatDescOd must be upto 40 character string    ##
##    emmeMatNumPt  : OPTIONAL : matrix location in Emme bank to write PKAD total transit trips table     -- if emmeOut variable is set to 'T', emmeMatPt must be value between 1 and 150         ##
##    emmeMatNamePt : OPTIONAL : 6-string matrix name in Emme bank for PKAD total transit trips table     -- if emmeOut variable is set to 'T', emmeMatNamePt must be 1 to 6 character string     ##
##    emmeMatDescPt : OPTIONAL : description of matrix in Emme bank for PKAD total transit trips table    -- if emmeOut variable is set to 'T', emmeMatDescPt must be upto 40 character string    ##
##    emmeMatNumOt  : OPTIONAL : matrix location in Emme bank to write OPAD total transit trips table     -- if emmeOut variable is set to 'T', emmeMatOt must be value between 1 and 150         ##
##    emmeMatNameOt : OPTIONAL : 6-string matrix name in Emme bank for OPAD total transit trips table     -- if emmeOut variable is set to 'T', emmeMatNameOt must be 1 to 6 character string     ##
##    emmeMatDescOt : OPTIONAL : description of matrix in Emme bank for OPAD total transit trips table    -- if emmeOut variable is set to 'T', emmeMatDescOt must be upto 40 character string    ##
##                                                                                                                                                                                                ##
## Examples:                                                                                                                                                                                      ##
## > transitPeakingPKOP()                                                                                                                                                                         ##
## > transitPeakingPKOP(emmeOut=T,emmeMatNumPw=50,emmeMatNamePw='pkdwtr',emmeMatDescPw='PKAD Walk to Transit Trips',                                                                              ##
##                                emmeMatNumOw=51,emmeMatNameOw='opdwtr',emmeMatDescOw='OPAD Walk to Transit Trips',                                                                              ##
##                                emmeMatNumPd=52,emmeMatNamePd='pkdptr',emmeMatDescPd='PKAD Drive to Transit Trips',                                                                             ##
##                                emmeMatNumOd=53,emmeMatNameOd='opdwtr',emmeMatDescOd='OPAD Drive to Transit Trips',                                                                             ##
##                                emmeMatNumPt=54,emmeMatNamePt='pkdttr',emmeMatDescPt='PKAD Total Transit Trips',                                                                                ##
##                                emmeMatNumOt=55,emmeMatNameOt='opdttr',emmeMatDescOt='OPAD Total Transit Trips')                                                                                ##
##                                                                                                                                                                                                ##
####################################################################################################################################################################################################

###########################################################
##             Calculating Transit Trips                 ##
###########################################################
transitPeakingPKOP <- function(emmeOut=F,emmeMatNumPw=0,emmeMatNamePw='',emmeMatDescPw='',
                                         emmeMatNumOw=0,emmeMatNameOw='',emmeMatDescOw='',
                                         emmeMatNumPd=0,emmeMatNamePd='',emmeMatDescPd='',
                                         emmeMatNumOd=0,emmeMatNameOd='',emmeMatDescOd='',
                                         emmeMatNumPt=0,emmeMatNamePt='',emmeMatDescPt='',
                                         emmeMatNumOt=0,emmeMatNameOt='',emmeMatDescOt='') {

  # Check Emme variables. Ends function and reports error message if variables are incorrect.
  stp <- emmeCheck(emmeOut,emmeMatNumPw,emmeMatNamePw,emmeMatDescPw)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumOw,emmeMatNameOw,emmeMatDescOw)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumPd,emmeMatNamePd,emmeMatDescPd)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumOd,emmeMatNameOd,emmeMatDescOd)
  if (stp > 0) { stop(call.=FALSE) }
  stp <- emmeCheck(emmeOut,emmeMatNumPt,emmeMatNamePt,emmeMatDescPt)
  if (stp > 0) { stop(call.=FALSE) }
    stp <- emmeCheck(emmeOut,emmeMatNumOt,emmeMatNameOt,emmeMatDescOt)
  if (stp > 0) { stop(call.=FALSE) }

  print ("Peaking PKAD and OPAD Transit")

  todTripsTransitPKOP()
  
  ### Calculations for Drive to Transit (PnR) ###
       # PKAD peak Direction
       mf.pkdir.prtrtd <- (mf.hwprtrtd.pk) * (peakHBW.TRANSIT.PA[7] + peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[16] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18]) +
                          (mf.hoprtrtd.pk) * (peakHBO.TRANSIT.PA[7] + peakHBO.TRANSIT.PA[8] + peakHBO.TRANSIT.PA[9] + peakHBO.TRANSIT.AP[16] + peakHBO.TRANSIT.AP[17] + peakHBO.TRANSIT.AP[18]) +
                          (mf.hsprtrtd.pk) * (peakHBS.TRANSIT.PA[7] + peakHBS.TRANSIT.PA[8] + peakHBS.TRANSIT.PA[9] + peakHBS.TRANSIT.AP[16] + peakHBS.TRANSIT.AP[17] + peakHBS.TRANSIT.AP[18]) +
                          (mf.hrprtrtd.pk) * (peakHBR.TRANSIT.PA[7] + peakHBR.TRANSIT.PA[8] + peakHBR.TRANSIT.PA[9] + peakHBR.TRANSIT.AP[16] + peakHBR.TRANSIT.AP[17] + peakHBR.TRANSIT.AP[18]) +
                          (mf.hcprtrtd.pk) * (peakCollege.TRANSIT.PA[7] + peakCollege.TRANSIT.PA[8] + peakCollege.TRANSIT.PA[9] + peakCollege.TRANSIT.AP[16] + peakCollege.TRANSIT.AP[17] + peakCollege.TRANSIT.AP[18])

       # PKAD off-peak Direction
       ### Calculations for Drive to Transit (PnR) ###
       mf.opdir.prtrtd <- (mf.hwprtrtd.pk) * (peakHBW.TRANSIT.AP[7] + peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[16] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]) +
                          (mf.hoprtrtd.pk) * (peakHBO.TRANSIT.AP[7] + peakHBO.TRANSIT.AP[8] + peakHBO.TRANSIT.AP[9] + peakHBO.TRANSIT.PA[16] + peakHBO.TRANSIT.PA[17] + peakHBO.TRANSIT.PA[18]) +
                          (mf.hsprtrtd.pk) * (peakHBS.TRANSIT.AP[7] + peakHBS.TRANSIT.AP[8] + peakHBS.TRANSIT.AP[9] + peakHBS.TRANSIT.PA[16] + peakHBS.TRANSIT.PA[17] + peakHBS.TRANSIT.PA[18]) +
                          (mf.hrprtrtd.pk) * (peakHBR.TRANSIT.AP[7] + peakHBR.TRANSIT.AP[8] + peakHBR.TRANSIT.AP[9] + peakHBR.TRANSIT.PA[16] + peakHBR.TRANSIT.PA[17] + peakHBR.TRANSIT.PA[18]) +
                          (mf.hcprtrtd.pk) * (peakCollege.TRANSIT.AP[7] + peakCollege.TRANSIT.AP[8] + peakCollege.TRANSIT.AP[9] + peakCollege.TRANSIT.PA[16] + peakCollege.TRANSIT.PA[17] + peakCollege.TRANSIT.PA[18])

       #Calculate PKAD Drive to Transit Trips
       mf.PKAD.prtrtd <- t(mf.pkdir.prtrtd) + mf.opdir.prtrtd

       #Calculate OPAD Drive to Transit Trips
       mf.OPAD.prtrtd <- (mf.hwprtrtd.op +
                          mf.hsprtrtd.op +
                          mf.hrprtrtd.op +
                          mf.hoprtrtd.op +
                          mf.hcprtrtd.op)

  ### Calculations for Walk to Transit ###
       # PKAD peak Direction
       mf.pkdir.tr     <- (mf.hwtr.pk)   * (peakHBW.TRANSIT.PA[7] + peakHBW.TRANSIT.PA[8] + peakHBW.TRANSIT.PA[9] + peakHBW.TRANSIT.AP[16] + peakHBW.TRANSIT.AP[17] + peakHBW.TRANSIT.AP[18]) +
                          (mf.hotr.pk)   * (peakHBO.TRANSIT.PA[7] + peakHBO.TRANSIT.PA[8] + peakHBO.TRANSIT.PA[9] + peakHBO.TRANSIT.AP[16] + peakHBO.TRANSIT.AP[17] + peakHBO.TRANSIT.AP[18]) +
                          (mf.hstr.pk)   * (peakHBS.TRANSIT.PA[7] + peakHBS.TRANSIT.PA[8] + peakHBS.TRANSIT.PA[9] + peakHBS.TRANSIT.AP[16] + peakHBS.TRANSIT.AP[17] + peakHBS.TRANSIT.AP[18]) +
                          (mf.hrtr.pk)   * (peakHBR.TRANSIT.PA[7] + peakHBR.TRANSIT.PA[8] + peakHBR.TRANSIT.PA[9] + peakHBR.TRANSIT.AP[16] + peakHBR.TRANSIT.AP[17] + peakHBR.TRANSIT.AP[18]) +
                          (mf.nhwtr.pk)  * (peakNHW.TRANSIT.PA[7] + peakNHW.TRANSIT.PA[8] + peakNHW.TRANSIT.PA[9] + peakNHW.TRANSIT.AP[16] + peakNHW.TRANSIT.AP[17] + peakNHW.TRANSIT.AP[18]) +
                          (mf.nhnwtr.pk) * (peakNHNW.TRANSIT.OD[7] + peakNHNW.TRANSIT.OD[8] + peakNHNW.TRANSIT.OD[9] + peakNHNW.TRANSIT.OD[16] + peakNHNW.TRANSIT.OD[17] + peakNHNW.TRANSIT.OD[18]) / 2

       # Add college and school trips
       mf.pkdir.tr     <- (mf.hctr.pk)   * (peakCollege.TRANSIT.PA[7] + peakCollege.TRANSIT.PA[8] + peakCollege.TRANSIT.PA[9] + peakCollege.TRANSIT.AP[16] + peakCollege.TRANSIT.AP[17] + peakCollege.TRANSIT.AP[18]) +
                          (mf.schtr)     * (peakSchool.TRANSIT.PA[7] + peakSchool.TRANSIT.PA[8] + peakSchool.TRANSIT.PA[9] + peakSchool.TRANSIT.AP[16] + peakSchool.TRANSIT.AP[17] + peakSchool.TRANSIT.AP[18]) +
                          (mf.pkdir.tr)

       # PKAD off-peak Direction
       mf.opdir.tr     <- (mf.hwtr.pk)   * (peakHBW.TRANSIT.AP[7] + peakHBW.TRANSIT.AP[8] + peakHBW.TRANSIT.AP[9] + peakHBW.TRANSIT.PA[16] + peakHBW.TRANSIT.PA[17] + peakHBW.TRANSIT.PA[18]) +
                          (mf.hotr.pk)   * (peakHBO.TRANSIT.AP[7] + peakHBO.TRANSIT.AP[8] + peakHBO.TRANSIT.AP[9] + peakHBO.TRANSIT.PA[16] + peakHBO.TRANSIT.PA[17] + peakHBO.TRANSIT.PA[18]) +
                          (mf.hstr.pk)   * (peakHBS.TRANSIT.AP[7] + peakHBS.TRANSIT.AP[8] + peakHBS.TRANSIT.AP[9] + peakHBS.TRANSIT.PA[16] + peakHBS.TRANSIT.PA[17] + peakHBS.TRANSIT.PA[18]) +
                          (mf.hrtr.pk)   * (peakHBR.TRANSIT.AP[7] + peakHBR.TRANSIT.AP[8] + peakHBR.TRANSIT.AP[9] + peakHBR.TRANSIT.PA[16] + peakHBR.TRANSIT.PA[17] + peakHBR.TRANSIT.PA[18]) +
                          (mf.nhwtr.pk)  * (peakNHW.TRANSIT.AP[7] + peakNHW.TRANSIT.AP[8] + peakNHW.TRANSIT.AP[9] + peakNHW.TRANSIT.PA[16] + peakNHW.TRANSIT.PA[17] + peakNHW.TRANSIT.PA[18]) +
                          (mf.nhnwtr.pk) * (peakNHNW.TRANSIT.OD[7] + peakNHNW.TRANSIT.OD[8] + peakNHNW.TRANSIT.OD[9] + peakNHNW.TRANSIT.OD[16] + peakNHNW.TRANSIT.OD[17] + peakNHNW.TRANSIT.OD[18]) / 2

       # Add college and school trips
       mf.opdir.tr     <- (mf.hctr.pk)   * (peakCollege.TRANSIT.AP[7] + peakCollege.TRANSIT.AP[8] + peakCollege.TRANSIT.AP[9] + peakCollege.TRANSIT.PA[16] + peakCollege.TRANSIT.PA[17] + peakCollege.TRANSIT.PA[18]) +
                          (mf.schtr)     * (peakSchool.TRANSIT.AP[7] + peakSchool.TRANSIT.AP[8] + peakSchool.TRANSIT.AP[9] + peakSchool.TRANSIT.PA[16] + peakSchool.TRANSIT.PA[17] + peakSchool.TRANSIT.PA[18]) +
                          (mf.opdir.tr)

       #Calculate PKAD Walk to Transit Trips
       mf.PKAD.tr <- t(mf.pkdir.tr) + mf.opdir.tr

       #Calculate OPAD Walk to Transit Trips
       mf.OPAD.tr <- (mf.hwtr.op +
                      mf.hstr.op +
                      mf.hrtr.op +
                      mf.hotr.op +
                      mf.nhwtr.op +
                      mf.nhnwtr.op +
                      mf.hctr.op +
                     (mf.schtr - (mf.schtr * (peakSchool.TRANSIT.PA[7] + peakSchool.TRANSIT.PA[8] + peakSchool.TRANSIT.PA[9] + peakSchool.TRANSIT.AP[16] + peakSchool.TRANSIT.AP[17] + peakSchool.TRANSIT.AP[18] +
                                              peakSchool.TRANSIT.AP[7] + peakSchool.TRANSIT.AP[8] + peakSchool.TRANSIT.AP[9] + peakSchool.TRANSIT.PA[16] + peakSchool.TRANSIT.PA[17] + peakSchool.TRANSIT.PA[18]))))

       # If replacement of HBC trips with U of O tour-based model trips is desired...
       if (uo_replace == TRUE) {
         
         ##################################################################################################
         ### FOR TESTING PURPOSES - REMOVE WHEN FINALIZING ################################################
         print(paste("mf.PKAD.tr total pre          : ", round(sum(mf.PKAD.tr),0),sep=""))            #
         print(paste("mf.PKAD.tr total pre to UO    : ", round(sum(mf.PKAD.tr[,uo_zones]),0),sep="")) #
         print(paste("mf.PKAD.tr total pre from UO  : ", round(sum(mf.PKAD.tr[uo_zones,]),0),sep="")) #
         ##################################################################################################
         
         # modes (tr) = uo_trips$tripMode = 9+10
         swap_modes <- c(9:10)
         mf.PKAD.tr <- swap_uo_trips(mf.PKAD.tr, uo_zones, swap_modes,
                                     startHour=15, stopHour=18,
                                     uo_trips) # PM demand in PM peak direction
         mf.PKAD.tr <- swap_uo_trips(mf.PKAD.tr, uo_zones, swap_modes,
                                     startHour=6, stopHour=9,
                                     uo_trips, reverse=T) # AM demand in PM peak direction
         for (h in list(c(0, 6), c(9, 15), c(18, 24))) {
           mf.OPAD.tr <- swap_uo_trips(mf.PKAD.tr, uo_zones, swap_modes,
                                       startHour=h[1], stopHour=h[2],
                                       uo_trips) # off-peak demand
         }
         
         ##################################################################################################
         ### FOR TESTING PURPOSES - REMOVE WHEN FINALIZING ################################################
         print(paste("mf.PKAD.tr total post          : ", round(sum(mf.PKAD.tr),0),sep=""))            #
         print(paste("mf.PKAD.tr total post to UO    : ", round(sum(mf.PKAD.tr[,uo_zones]),0),sep="")) #
         print(paste("mf.PKAD.tr total post from UO  : ", round(sum(mf.PKAD.tr[uo_zones,]),0),sep="")) #
         ##################################################################################################
         
         ##################################################################################################
         ### FOR TESTING PURPOSES - REMOVE WHEN FINALIZING ################################################
         print(paste("mf.PKAD.prtrtd total pre          : ", round(sum(mf.PKAD.prtrtd),0),sep=""))            #
         print(paste("mf.PKAD.prtrtd total pre to UO    : ", round(sum(mf.PKAD.prtrtd[,uo_zones]),0),sep="")) #
         print(paste("mf.PKAD.prtrtd total pre from UO  : ", round(sum(mf.PKAD.prtrtd[uo_zones,]),0),sep="")) #
         ##################################################################################################

         # modes (prtr) = uo_trips$tripMode = 11+12
         swap_modes <- c(11:12)
         mf.PKAD.prtrtd <- swap_uo_trips(mf.PKAD.prtrtd, uo_zones, swap_modes,
                                         startHour=15, stopHour=18,
                                         uo_trips.prtr) # PM demand in PM peak direction
         mf.PKAD.prtrtd <- swap_uo_trips(mf.PKAD.prtrtd, uo_zones, swap_modes,
                                         startHour=6, stopHour=9,
                                         uo_trips.prtr, reverse=T) # AM demand in PM peak direction
         for (h in list(c(0, 6), c(9, 15), c(18, 24))) {
           mf.OPAD.prtrtd <- swap_uo_trips(mf.OPAD.prtrtd, uo_zones, swap_modes,
                                           startHour=h[1], stopHour=h[2],
                                           uo_trips.prtr) # off-peak demand
         }
         
         ##################################################################################################
         ### FOR TESTING PURPOSES - REMOVE WHEN FINALIZING ################################################
         print(paste("mf.PKAD.prtrtd total post          : ", round(sum(mf.PKAD.prtrtd),0),sep=""))            #
         print(paste("mf.PKAD.prtrtd total post to UO    : ", round(sum(mf.PKAD.prtrtd[,uo_zones]),0),sep="")) #
         print(paste("mf.PKAD.prtrtd total post from UO  : ", round(sum(mf.PKAD.prtrtd[uo_zones,]),0),sep="")) #
         ##################################################################################################
       }

  ####---- Calculate total PKAD and OPAD ----####
  mf.PKAD.total  <- mf.PKAD.tr + mf.PKAD.prtrtd
  mf.OPAD.total  <- mf.OPAD.tr + mf.OPAD.prtrtd

  # Reporting for testing
  print(paste("Total PKAD walk transit trips    : ",round(sum(mf.PKAD.tr),0),sep=''))
  print(paste("Total PKAD PnR transit trips     : ",round(sum(mf.PKAD.prtrtd),0),sep=''))
  print(paste("Total PKAD transit trips         : ",round(sum(mf.PKAD.total),0),sep=''))
  print(paste("Total OPAD walk transit trips    : ",round(sum(mf.OPAD.tr),0),sep=''))
  print(paste("Total OPAD PnR transit trips     : ",round(sum(mf.OPAD.prtrtd),0),sep=''))
  print(paste("Total OPAD transit trips         : ",round(sum(mf.OPAD.total),0),sep=''))
  print(paste("Total daily walk transit trips   : ",round(sum(mf.PKAD.tr),0) + round(sum(mf.OPAD.tr),0),sep=''))
  print(paste("Total daily PnR transit trips    : ",round(sum(mf.PKAD.prtrtd),0) + round(sum(mf.OPAD.prtrtd),0),sep=''))
  print(paste("Total daily transit trips        : ",round(sum(mf.PKAD.total + mf.OPAD.total),0),sep=''))

  rm(list=ls(pattern = "mf\\.ap\\."))
  rm(list=ls(pattern = "mf\\.pa\\."))

  # Sum hourly tables for final O->D table for district summaries
  print ("Creating 8-district summary for PKAD transit trips")
  options(warn=-1)  ## Turns off warning messages associated with distsum program
  mf.distemp <<- mf.PKAD.tr
  distsum ("mf.distemp", "PKAD walk to transit trips", "ga", 3, "peakTRANSIT", project, initials)
  mf.distemp <<- mf.PKAD.prtrtd
  distsum ("mf.distemp", "PKAD drive to transit trips", "ga", 3, "peakTRANSIT", project, initials)
  mf.distemp <<- mf.PKAD.total
  distsum ("mf.distemp", "PKAD total transit trips", "ga", 3, "peakTRANSIT", project, initials)

  print ("Creating 8-district summary for OPAD transit trips")
  mf.distemp <<- mf.OPAD.tr
  distsum ("mf.distemp", "OPAD walk to transit trips", "ga", 3, "peakTRANSIT", project, initials)
  mf.distemp <<- mf.OPAD.prtrtd
  distsum ("mf.distemp", "OPAD drive to transit trips", "ga", 3, "peakTRANSIT", project, initials)
  mf.distemp <<- mf.OPAD.total
  distsum ("mf.distemp", "OPAD total transit trips", "ga", 3, "peakTRANSIT", project, initials)

  # Output trip table to Emme if defined
  if (isTRUE(emmeOut)) { 
     print("Creating Emme O->D tables for PKAD & OPAD transit trips")
     writeEmme(mf.PKAD.tr,assignbank,emmeMatNamePw,emmeMatDescPw,emmeMatNumPw)
     writeEmme(mf.OPAD.tr,assignbank,emmeMatNameOw,emmeMatDescOw,emmeMatNumOw)
     writeEmme(mf.PKAD.prtrtd,assignbank,emmeMatNamePd,emmeMatDescPd,emmeMatNumPd)
     writeEmme(mf.OPAD.prtrtd,assignbank,emmeMatNameOd,emmeMatDescOd,emmeMatNumOd)
     writeEmme(mf.PKAD.total,assignbank,emmeMatNamePt,emmeMatDescPt,emmeMatNumPt)
     writeEmme(mf.OPAD.total,assignbank,emmeMatNameOt,emmeMatDescOt,emmeMatNumOt)
  }

  mf.distemp <<- 0
}
