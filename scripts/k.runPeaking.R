# k.runPeaking.R
# Created by PGB 5/2011

# To run this program manually, enter the following line at the
# Cygwin command prompt from within the ./model/peak folder:
#    R -q --slave --vanilla --args ../../k.model_setup.R < k.runPeaking.R

if (length(commandArgs(TRUE)) > 0) { source(commandArgs()[length(commandArgs())]) }

sink(paste(project.dir,"/logs/peaking_report.log",sep=''), append = FALSE, type = c("output", "message"))
options(warn=-1)

#system("echo '    Calculating external trips for peaking")
#
## Calculate External AWD matrix
#source(paste(R.path,"peak/k.runExternals.R",sep="/"))

system("echo '    Loading data for peaking")

# Load distsum utility and 8-district ensemble for producing reports
source(paste(R.path,"distsum.R",sep="/"))
load_ensemble(proj.inputs,"ga")

# Load daily matrices
print("Loading daily matrices and peaking factors")
  source(paste(R.path,"peak/k.loadDailyMatrices.R", sep="/"))

# Define csv files that contains peaking factors....load files into R
  peakingtable      <- paste(R.path,"peak/peakingFactors/peakingFactorsAllday.csv", sep="/")
#  peaking15minTable <- paste(R.path,"peak/peakingFactors/peakingFactors24hr15min_smooth.csv", sep="/")
  source(paste(R.path,"peak/k.loadPeakingFactors.R", sep="/"))

# Execute peaking programs...for a detailed explanation of peaking functions, view comments in k.peakingFunctions.R
print("Executing peaking programs")
  source(paste(R.path,"peak/k.peakingPrep.R", sep="/"))
  source(paste(R.path,"peak/k.auxPeakingFunctions.R", sep="/"))
  source(paste(R.path,"peak/k.peakingFunctionSOV.R", sep="/"))
  source(paste(R.path,"peak/k.peakingFunctionHOV.R", sep="/"))
  source(paste(R.path,"peak/k.peakingFunctionTRUCK.R", sep="/"))
  source(paste(R.path,"peak/k.peakingFunctionTRANSIT.R", sep="/"))
  source(paste(R.path,"peak/k.peakingFunctionTRANSITPKOP.R", sep="/"))

  #--------------------------------------------------------------------------------------------------------------
  # Peaking of standard auto trip tables
  #--------------------------------------------------------------------------------------------------------------

  ## AM 2-hr auto/truck peaking
  system("echo '    >> AM 2-hr auto/truck peaking'")
  sovPeaking(7,9,newReport=T,emmeOut=T,emmeMatNum=40,emmeMatName='am2sov',emmeMatDesc='AM2 SOV Trips')
  hovPeaking(7,9,newReport=T,emmeOut=T,emmeMatNum=41,emmeMatName='am2hov',emmeMatDesc='AM2 HOV Trips')
  truckPeaking(7,9,newReport=T,emmeOut=T,emmeMatNumH=42,emmeMatNameH='am2Hpe',emmeMatDescH='AM2 Heavy Truck Trips',
                                           emmeMatNumM=43,emmeMatNameM='am2Mpe',emmeMatDescM='AM2 Medium Truck Trips')

  ## AM 2-hr assignmment to produce AM 2-hr skims for convergence test
  emmeBASHpeak(assignbank, paste("Emme -ng 000 -m ",emmeAssignCmd,sep=''))
  emmeBASHpeak(assignbank, paste("Emme -ng 000 -m ",emmeSkimckCmd,sep=''))
  system(paste(emmePythonPath,pythonHistogram,assignbank,sep=' '))
  emmeBASHpeak(assignbank, "Emme -ng 000 -m tt_histogram_matdel")

  ## MD 1-hr auto/truck peaking
  system("echo '    >> MD 1-hr auto/truck peaking'")
  sovPeaking(12,13,newReport=F,emmeOut=T,emmeMatNum=44,emmeMatName='md1sov',emmeMatDesc='MD1 SOV Trips')
  hovPeaking(12,13,newReport=F,emmeOut=T,emmeMatNum=45,emmeMatName='md1hov',emmeMatDesc='MD1 HOV Trips')
  truckPeaking(12,13,newReport=F,emmeOut=T,emmeMatNumH=46,emmeMatNameH='md1Hpe',emmeMatDescH='MD1 Heavy Truck Trips',
                                           emmeMatNumM=47,emmeMatNameM='md1Mpe',emmeMatDescM='MD1 Medium Truck Trips')

  ## PM 2-hr auto/truck peaking
  system("echo '    >> PM 2-hr auto/truck peaking'")
  sovPeaking(16,18,newReport=T,emmeOut=T,emmeMatNum=48,emmeMatName='pm2sov',emmeMatDesc='PM2 SOV Trips')
  hovPeaking(16,18,newReport=T,emmeOut=T,emmeMatNum=49,emmeMatName='pm2hov',emmeMatDesc='PM2 HOV Trips')
  truckPeaking(16,18,newReport=T,emmeOut=T,emmeMatNumH=50,emmeMatNameH='pm2Hpe',emmeMatDescH='PM2 Heavy Truck Trips',
                                           emmeMatNumM=51,emmeMatNameM='pm2Mpe',emmeMatDescM='PM2 Medium Truck Trips')


  #--------------------------------------------------------------------------------------------------------------
  # Peaking of standard transit tables
  #--------------------------------------------------------------------------------------------------------------

  ## PKAD / OPAD transit peaking
  system("echo '    >> PKAD / OPAD transit peaking'")
  transitPeakingPKOP(emmeOut=T,emmeMatNumPw=52,emmeMatNamePw='pkdwtr',emmeMatDescPw='PKAD Transit Trips - Walk Access',
                               emmeMatNumOw=53,emmeMatNameOw='opdwtr',emmeMatDescOw='OPAD Transit Trips - Walk Access',
                               emmeMatNumPd=54,emmeMatNamePd='pkdptr',emmeMatDescPd='PKAD Transit Trips - Park & Ride',
                               emmeMatNumOd=55,emmeMatNameOd='opdptr',emmeMatDescOd='OPAD Transit Trips - Park & Ride',
                               emmeMatNumPt=56,emmeMatNamePt='pkdttr',emmeMatDescPt='PKAD Total Transit Trips',
                               emmeMatNumOt=57,emmeMatNameOt='opdttr',emmeMatDescOt='OPAD Total Transit Trips')


  #--------------------------------------------------------------------------------------------------------------
  # Miscellaneous auto and transit peaking options
  #--------------------------------------------------------------------------------------------------------------

  ## AM 1-hr transit peaking
  system("echo '    >> AM 1-hr transit peaking'")
  transitPeaking(7,8,newReport=T,emmeOut=T,emmeMatNumTw=58,emmeMatNameTw='am1wtr',emmeMatDescTw='AM1 Transit Trips - Walk Access',
                                           emmeMatNumTd=59,emmeMatNameTd='am1ptr',emmeMatDescTd='AM1 Transit Trips - Park & Ride',
                                           emmeMatNumTr=60,emmeMatNameTr='am1ttr',emmeMatDescTr='AM1 Total Transit Trips')

  ## PM 1-hr transit peaking
  system("echo '    >> PM 1-hr transit peaking'")
  transitPeaking(17,18,newReport=F,emmeOut=T,emmeMatNumTw=61,emmeMatNameTw='pm1wtr',emmeMatDescTw='PM1 Transit Trips - Walk Access',
                                             emmeMatNumTd=62,emmeMatNameTd='pm1ptr',emmeMatDescTd='PM1 Transit Trips - Park & Ride',
                                             emmeMatNumTr=63,emmeMatNameTr='pm1ttr',emmeMatDescTr='PM1 Total Transit Trips')

# Remove any hourly trip tables by uncommenting out the following line
  system (paste("rm ", project.dir,"/model/peak/mf.*.dat", sep=""))
